$id: "https://gitlab.com/gitlab-org/security-products/sast-rules/-/raw/main/.yamllint"
$schema: "http://json-schema.org/draft-07/schema#"
title: "GitLab Semgrep Rule"
description: >-
  Custom extension of the official Semgrep rule specification to perform additional validations and checks for
  automatic linting.
type: "object"
properties:
  rules:
    type: "array"
    items:
      type: "object"
      properties:
        id:
          title: "Rule ID"
          description: "Rule ID to attach to findings"
          type: "string"
          pattern: "^(c|cpp|csharp|go|java|javascript|python|scala|kotlin|swift|oc|ruby|php|rules)_[a-zA-Z0-9_.-]+$"
        languages:
          title: "Rule Languages"
          description: "Languages the rule is intended for"
          type: "array"
          items:
            type: "string"
            enum:
              - "c"
              - "cpp"
              - "csharp"
              - "go"
              - "java"
              - "javascript"
              - "python"
              - "typescript"
              - "scala"
              - "kotlin"
              - "swift"
              - "generic"  # "oc" is replaced by "generic" because it is not supported by semgrep
              - "ruby"
              - "php"
          maxItems: 2
          uniqueItems: true
        message:
          title: "Rule Message"
          description: "A detailed description of the issue with examples and recommended mitigations"
        metadata:
          title: "Rule Metadata"
          description: "Related identifiers and short description"
          type: "object"
          properties:
            shortDescription:
              type: "string"
              minLength: 16
              maxLength: 128
            cwe:
              title: "CWE ID"
              description: "A relevant Common Weakness Enumeration ID for the issue (https://cwe.mitre.org/)"
              type: "string"
              pattern: "^CWE-\\d{1,4}$"
            owasp:
              title: "OWASP Top 10 Category"
              description: |-
                A relevant OWASP TOP 10 category from 2017 and 2021.

                2021: https://owasp.org/Top10/
                2017: https://owasp.org/www-project-top-ten/2017/Top_10.html
              anyOf:
                - type: "string"
                  enum: &owaspCategories
                    - "A01:2021-Broken Access Control"
                    - "A02:2021-Cryptographic Failures"
                    - "A03:2021-Injection"
                    - "A04:2021-Insecure Design"
                    - "A05:2021-Security Misconfiguration"
                    - "A06:2021-Vulnerable and Outdated Components"
                    - "A07:2021-Identification and Authentication Failures"
                    - "A08:2021-Software and Data Integrity Failures"
                    - "A09:2021-Security Logging and Monitoring Failures"
                    - "A10:2021-Server-Side Request Forgery"
                    - "A1:2017-Injection"
                    - "A2:2017-Broken Authentication"
                    - "A3:2017-Sensitive Data Exposure"
                    - "A4:2017-XML External Entities (XXE)"
                    - "A5:2017-Broken Access Control"
                    - "A6:2017-Security Misconfiguration"
                    - "A7:2017-Cross-Site Scripting (XSS)"
                    - "A8:2017-Insecure Deserialization"
                    - "A9:2017-Using Components with Known Vulnerabilities"
                    - "A10:2017-Insufficient Logging & Monitoring"
                - type: "array"
                  items:
                    type: "string"
                    minItems: 2
                    enum: *owaspCategories
            security-severity:
              title: "The severity of a vulnerability. This replaces severity"
              type: string
          required:
            - "shortDescription"
            - "cwe"
            - "security-severity"
      required:
        - "id"
        - "languages"
        - "message"
        - "metadata"
        - "severity"
    maxItems: 1
