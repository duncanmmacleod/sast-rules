#!/usr/bin/env bash

set -e

badFiles=$(find . -type f -name "rule-*.yaml")

if [ "$badFiles" != "" ]; then
  printf "Rule files with .yaml extension detected:\n\n%s\n\n" "$badFiles"
  echo "Please rename files to have .yml extension."
  exit 1
fi

echo "No rule files with wrong .yaml extension detected."
