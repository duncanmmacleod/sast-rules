// License: LGPL-3.0 License (c) find-sec-bugs
package random

import org.apache.commons.lang.RandomStringUtils
import org.apache.commons.lang.math.RandomUtils

object PseudoRandom {
    // ruleid: kotlin_random_rule-PseudoRandom
    var randomVal: String = java.lang.Long.toHexString(java.util.Random().nextLong())

    fun generateSecretToken(): String {
        // ruleid: kotlin_random_rule-PseudoRandom
        val r: java.util.Random = java.util.Random()
        // ruleid: kotlin_random_rule-PseudoRandom
        return java.lang.Long.toHexString(r.nextLong())
    }

    fun count(): String {
        // ruleid: kotlin_random_rule-PseudoRandom
        return RandomStringUtils.random(10)
    }

    fun getRandomVal(): Long {
        // ruleid: kotlin_random_rule-PseudoRandom
        return RandomUtils.nextLong()
    }

    fun getRandomBytes(size: Int): ByteArray {
        val byteArray = ByteArray(size / 8)
        // ruleid: kotlin_random_rule-PseudoRandom
        java.util.Random().nextBytes(byteArray)
        return byteArray
    }
}
