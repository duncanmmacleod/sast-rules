# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://find-sec-bugs.github.io/bugs.htm
# yamllint enable
---
rules:
  - id: "kotlin_password_rule-HardcodePassword"
    languages:
      - "kotlin"
    message: |
      Passwords should not be kept in the source code. The source code can be widely shared in an
      enterprise environment, and is certainly shared in open source. To be managed safely, passwords
      and secret keys should be stored in separate configuration files or keystores.
    severity: "ERROR"
    metadata:
      shortDescription: "Use of hard-coded password"
      category: "security"
      cwe: "CWE-259"
      owasp:
        - "A2:2017-Broken Authentication"
        - "A07:2021-Identification and Authentication Failures"
      technology:
        - "kotlin"
      security-severity: "CRITICAL"

    patterns:
      - pattern-either:
          - pattern-inside: |
              val $PWD: CharArray = ...
              ...
          - pattern-inside: |
              val $PWD: String = ...
              ...
          - pattern-inside: |
              class $CLAZZ {
              val $PWD : CharArray = ...
                ...
              }
          - pattern-inside: |
              class $CLAZZ {
              val $PWD : String = ...
                ...
              }
          - pattern-inside: |
              $S = SymmetricEncryptionConfig(...)
              ...
      - pattern-either:
          - pattern: "java.security.KeyStore.PasswordProtection(\"...\")"
          - pattern: "java.security.KeyStore.PasswordProtection(\"...\".$FOO(...))"
          - pattern: "java.security.KeyStore.PasswordProtection($PWD)"
          - pattern: "java.security.KeyStore.PasswordProtection($PWD.$FOO(...))"
          - pattern: "java.security.KeyStore.getInstance(...).load(..., \"...\")"
          - pattern: "java.security.KeyStore.getInstance(...).load(..., \"...\".$FOO(...))"
          - pattern: "java.security.KeyStore.getInstance(...).load(..., $PWD)"
          - pattern: "java.security.KeyStore.getInstance(...).load(..., $PWD.$FOO(...))"
          - pattern: "($KS: java.security.KeyStore).load(..., \"...\")"
          - pattern: "($KS: java.security.KeyStore).load(..., \"...\".$FOO(...))"
          - pattern: "($KS: java.security.KeyStore).load(..., $PWD)"
          - pattern: "($KS: java.security.KeyStore).load(..., $PWD.$FOO(...))"
          - pattern: "KeyManagerFactory.getInstance(...).init(..., $PWD)"
          - pattern: "KeyManagerFactory.getInstance(...).init(..., $PWD.$FOO(...))"
          - pattern: "KeyManagerFactory.getInstance(...).init(..., \"...\")"
          - pattern: "KeyManagerFactory.getInstance(...).init(..., \"...\".$FOO(...))"
          - pattern: "($KMF: KeyManagerFactory).init(..., $PWD)"
          - pattern: "($KMF: KeyManagerFactory).init(..., $PWD.$FOO(...))"
          - pattern: "($KMF: KeyManagerFactory).init(..., \"...\")"
          - pattern: "($KMF: KeyManagerFactory).init(..., \"...\".$FOO(...))"
          - pattern: "PBEKeySpec(\"...\", ...)"
          - pattern: "PBEKeySpec(\"...\".$FOO(...), ...)"
          - pattern: "PBEKeySpec($PWD, ...)"
          - pattern: "PBEKeySpec($PWD.$FOO(...), ...)"
          - pattern: "PasswordAuthentication(\"...\", \"...\")"
          - pattern: "PasswordAuthentication(\"...\", \"...\".$FOO(...))"
          - pattern: "PasswordAuthentication(\"...\", $PWD)"
          - pattern: "PasswordAuthentication(\"...\", $PWD.$FOO(...))"
          - pattern: "($CB: PasswordCallback).setPassword(\"...\")"
          - pattern: "($CB: PasswordCallback).setPassword(\"...\".$FOO(...))"
          - pattern: "($CB: PasswordCallback).setPassword($PWD)"
          - pattern: "($CB: PasswordCallback).setPassword($PWD.$FOO(...))"
          - pattern: "KerberosKey(...,\"...\",...)"
          - pattern: "KerberosKey(...,\"...\".$FOO(...),...)"
          - pattern: "KerberosKey(...,$PWD,...)"
          - pattern: "KerberosKey(...,$PWD.$FOO(...),...)"
          - pattern: "java.sql.DriverManager.getConnection(..., \"...\")"
          - pattern: "java.sql.DriverManager.getConnection(..., \"...\".$FOO(...))"
          - pattern: "java.sql.DriverManager.getConnection(..., $PWD)"
          - pattern: "java.sql.DriverManager.getConnection(..., $PWD.$FOO(...))"
          - pattern: "io.vertx.ext.web.handler.CSRFHandler.create(..., \"...\")"
          - pattern: "io.vertx.ext.web.handler.CSRFHandler.create(..., \"...\".$FOO(...))"
          - pattern: "io.vertx.ext.web.handler.CSRFHandler.create(..., $PWD)"
          - pattern: "io.vertx.ext.web.handler.CSRFHandler.create(..., $PWD.$FOO(...))"
          - pattern: "$S.setPassword($PWD)"
          - pattern: "$S.setPassword($PWD.$FOO(...))"
          - pattern: "$S.setPassword(\"...\")"

# - metavariable-regex:
#     metavariable: "$PWD"
#     regex: "(?i).*(pass|pwd|psw|secret|key|cipher|crypt|des|aes|mac|private|sign|cert).*"
