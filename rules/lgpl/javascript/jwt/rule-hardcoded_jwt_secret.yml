# yamllint disable
# License: GNU Lesser General Public License v3.0
# source (original): https://github.com/ajinabraham/njsscan/blob/master/njsscan/rules/semantic_grep/jwt/jwt_hardcoded.yaml
# hash: e7a0a61
# yamllint enable
---
rules:
- id: "rules_lgpl_javascript_jwt_rule-hardcoded-jwt-secret"
  patterns:
  - pattern-either:
    - pattern-inside: |
        const $JWT = require("jsonwebtoken");
        ...
    - pattern-inside: |
        const $JOSE = require("jose");
        ...
    - pattern-inside: |
        import $JWT from "jsonwebtoken"
        ...
    - pattern-inside: |
        import $JOSE from "jose"
        ...
  - pattern-either:
    - pattern: $JWT.sign($PAYLOAD, "...", ...)
    - pattern: $JWT.verify($PAYLOAD, "...", ...)
    - pattern: new $JOSE.SignJWT(...). ... .sign("...")
    - pattern: |
        var $TOKEN = new $JOSE.SignJWT(...). ...
        ...
        $TOKEN. ... .sign("...")
    - pattern: $JOSE.jwtVerify( $TKN, "...", ... )
    - patterns:
      - pattern-either:
        - patterns:
          - pattern-inside: |
              var $KEY = $X
                ...
          - pattern-either:
            - pattern: new $JOSE.SignJWT(...). ... .sign($KEY)
            - patterns:
              - pattern: |
                  var $TOKEN = new $JOSE.SignJWT(...). ...
                  ...
                  $TOKEN. ... .sign($KEY)
              - pattern: $TOKEN. ... .sign($KEY)
            - pattern: $JOSE.jwtVerify( $TKN, $KEY, ... )
        - pattern: new $JOSE.SignJWT(...). ... .sign($X)
        - patterns:
          - pattern: |
              var $TOKEN = new $JOSE.SignJWT(...). ...
              ...
              $TOKEN. ... .sign($X)
          - pattern: $TOKEN. ... .sign($X)
        - pattern: $JOSE.jwtVerify( $TKN, $X, ... )
      - metavariable-pattern:
          metavariable: $X
          pattern-either:
          - pattern: new TextEncoder().encode( "...",... )
          - pattern: Uint8Array.from("...", ...)
  message: |
    Hardcoded JWT secret or private key was found. Hardcoding secrets like JWT signing keys poses a significant security risk. 
    If the source code ends up in a public repository or is compromised, the secret is exposed. Attackers could then use the secret to 
    generate forged tokens and access the system. Store it properly in an environment variable.

    Here are some recommended safe ways to access JWT secrets:
      - Use environment variables to store the secret and access it in code instead of hardcoding. This keeps it out of source control.
      - Use a secrets management service to securely store and tightly control access to the secret. Applications can request the secret at runtime.
      - For local development, use a .env file that is gitignored and access the secret from process.env.

    sample code snippet of accessing JWT secret from env variables
    ```
     const token = jwt.sign(payload, process.env.SECRET, { algorithm: 'HS256' });
    ```
  languages:
  - "javascript"
  severity: "ERROR"
  metadata:
    owasp: 
    - "A3:2017-Sensitive Data Exposure"
    - "A02:2021-Cryptographic Failures"
    cwe: "CWE-798"
    shortDescription: "Use of hard-coded credentials"
    security-severity: "HIGH"
    category: "security"
