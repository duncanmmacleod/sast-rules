# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/xxe/transformerfactory-dtds-not-disabled.yaml
# yamllint enable
---
rules:
  - id: java_xxe_rule-TransformerfactoryDTDNotDisabled
    severity: "WARNING"
    languages:
      - "java"
    metadata:
      cwe: "CWE-611"
      shortDescription: "Improper restriction of XML external entity reference"
      owasp:
        - "A4:2017-XML External Entities (XXE)"
        - "A05:2021-Security Misconfiguration"
      security-severity: "MEDIUM"
      references:
        - "https://semgrep.dev/blog/2022/xml-security-in-java"
        - "https://semgrep.dev/docs/cheat-sheets/java-xxe/"
        - "https://blog.sonarsource.com/secure-xml-processor"
        - "https://xerces.apache.org/xerces2-j/features.html"
      category: "security"
      technology:
        - "java"
        - "xml"
      license: "Commons Clause License Condition v1.0[LGPL-2.1-only]"
    message: >
      This rule identifies configurations of the TransformerFactory class where
      DOCTYPE declarations are enabled. Enabling DOCTYPE declarations without
      proper restrictions can make your application vulnerable to XML External
      Entity (XXE) attacks. In an XXE attack, an attacker can exploit the
      processing of external entity references within an XML document to access
      internal files, conduct denial-of-service attacks, or SSRF (Server Side
      Request Forgery), potentially leading to sensitive information disclosure
      or system compromise.

      To mitigate XXE vulnerabilities related to the TransformerFactory instance in 
      Java, it is crucial to disable external DTD (Document Type Definition) and 
      external stylesheet processing. This can be accomplished by setting the 
      ACCESS_EXTERNAL_DTD and ACCESS_EXTERNAL_STYLESHEET factory attributes to an 
      empty string (""). This approach prevents the parser from fetching and processing 
      external references specified in the XML document.

      Secure Code Example:
      ```
      public static void configureTransformerFactory() throws TransformerConfigurationException {
        TransformerFactory factory = TransformerFactory.newInstance();
        // Disable access to external DTDs and stylesheets to prevent XXE
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
      }
      ```
    mode: taint
    pattern-sources:
      - by-side-effect: true
      # https://gitlab.com/gitlab-org/security-products/sast-rules/-/merge_requests/466#note_1821811133
        patterns:
          - pattern-either:
              - pattern: |
                  $FACTORY = javax.xml.transform.TransformerFactory.newInstance();
              - patterns:
                  - pattern: $FACTORY
                  - pattern-inside: |
                      class $C {
                        ...
                        $V $FACTORY = javax.xml.transform.TransformerFactory.newInstance();
                        ...
                      }
                  - pattern-not-inside: >
                      class $C {
                        ...
                        $V $FACTORY = javax.xml.transform.TransformerFactory.newInstance();
                        static {
                          ...
                          $FACTORY.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
                          ...
                          $FACTORY.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
                          ...
                        }
                        ...
                      }
                  - pattern-not-inside: >
                      class $C {
                        ...
                        $V $FACTORY = javax.xml.transform.TransformerFactory.newInstance();
                        static {
                          ...
                          $FACTORY.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
                          ...
                          $FACTORY.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
                          ...
                        }
                        ...
                      }
                  - pattern-not-inside: >
                      class $C {
                        ...
                        $V $FACTORY = javax.xml.transform.TransformerFactory.newInstance();
                        static {
                          ...
                          $FACTORY.setAttribute("=~/.*accessExternalDTD.*/", "");
                          ...
                          $FACTORY.setAttribute("=~/.*accessExternalStylesheet.*/", "");
                          ...
                        }
                        ...
                      }
                  - pattern-not-inside: >
                      class $C {
                        ...
                        $V $FACTORY = javax.xml.transform.TransformerFactory.newInstance();
                        static {
                          ...
                          $FACTORY.setAttribute("=~/.*accessExternalStylesheet.*/", "");
                          ...
                          $FACTORY.setAttribute("=~/.*accessExternalDTD.*/", "");
                          ...
                        }
                        ...
                      }
    pattern-sinks:
      - patterns:
          - pattern: $FACTORY.newTransformer(...);
    pattern-sanitizers:
      - by-side-effect: true
        pattern-either:
          - patterns:
              - pattern-either:
                  - pattern: >
                      $FACTORY.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET,
                      ""); ...

                      $FACTORY.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
                  - pattern: >
                      $FACTORY.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD,
                      "");

                      ...

                      $FACTORY.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
                  - pattern: >
                      $FACTORY.setAttribute("=~/.*accessExternalStylesheet.*/",
                      ""); ...

                      $FACTORY.setAttribute("=~/.*accessExternalDTD.*/", "");
                  - pattern: >
                      $FACTORY.setAttribute("=~/.*accessExternalDTD.*/", "");

                      ...

                      $FACTORY.setAttribute("=~/.*accessExternalStylesheet.*/", "");
              - focus-metavariable: $FACTORY
          - patterns:
              - pattern-either:
                  - pattern-inside: >
                      class $C {
                        ...
                        $T $M(...) {
                          ...
                          $FACTORY.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
                          ...
                          $FACTORY.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
                          ...
                        }
                        ...
                      }
                  - pattern-inside: >
                      class $C {
                        ...
                        $T $M(...) {
                          ...
                          $FACTORY.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
                          ...
                          $FACTORY.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
                          ...
                        }
                        ...
                      }
                  - pattern-inside: >
                      class $C {
                        ...
                        $T $M(...) {
                          ...
                          $FACTORY.setAttribute("=~/.*accessExternalStylesheet.*/", "");
                          ...
                          $FACTORY.setAttribute("=~/.*accessExternalDTD.*/", "");
                          ...
                        }
                        ...
                      }
                  - pattern-inside: >
                      class $C {
                        ...
                        $T $M(...) {
                          ...
                          $FACTORY.setAttribute("=~/.*accessExternalDTD.*/", "");
                          ...
                          $FACTORY.setAttribute("=~/.*accessExternalStylesheet.*/", "");
                          ...
                        }
                        ...
                      }
              - pattern: $M($X)
              - focus-metavariable: $X
