# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
  - id: "java_xxe_rule-XMLStreamRdr"
    languages:
      - "java"
    message: |
      External XML entities are a feature of XML parsers that allow 
      documents to contain references to other documents or data. This 
      feature can be abused to read files, communicate with external
      hosts, exfiltrate data, or cause a Denial of Service (DoS).
      In most XML parsers, the recommendation to protect against XXE is 
      to disable the doctype feature.
      Unfortunately use of the `XMLInputFactory` requires that the doctypes 
      feature be enabled. Instead the application can set the `ACCESS_EXTERNAL_DTD` 
      to an empty string and disable `javax.xml.stream.isSupportingExternalEntities`.

      Secure Code Example:
      ```
      // Create an XMLInputFactory
      XMLInputFactory factory = XMLInputFactory.newFactory();
      // Set the ACCESS_EXTERNAL_DTD property to an empty string so it won't access
      // entities using protocols
      // (ref:
      https://docs.oracle.com/javase/8/docs/api/javax/xml/XMLConstants.html#ACCESS_EXTERNAL_DTD)
      factory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
      // Additionally, disable support for resolving external entities
      factory.setProperty("javax.xml.stream.isSupportingExternalEntities", false);
      // Continue to work with the factory/stream parser
      ```

      For more information on XML security see OWASP's guide:
      https://cheatsheetseries.owasp.org/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.html#java
    patterns:
      - pattern-not-inside: |
          (javax.xml.stream.XMLInputFactory $FAC).setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
          ...
      - pattern-not-inside:
          patterns:
            - pattern-either:
                - pattern: |
                    (javax.xml.stream.XMLInputFactory $FAC).setProperty(XMLInputFactory.SUPPORT_DTD, $FALSE);
                    ...
                - pattern: |
                    (javax.xml.stream.XMLInputFactory $FAC).setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, $FALSE);
                    ...
                - pattern: |
                    (javax.xml.stream.XMLInputFactory $FAC).setProperty("javax.xml.stream.isSupportingExternalEntities", $FALSE);
                    ...
            - metavariable-pattern:
                metavariable: $FALSE
                pattern-either:
                  - pattern: |
                      false
                  - pattern: |
                      Boolean.FALSE
      - pattern: |
          (javax.xml.stream.XMLInputFactory $FAC).createXMLStreamReader(...)
    metadata:
      shortDescription: "Improper restriction of XML external entity reference ('XXE')"
      category: "security"
      cwe: "CWE-611"
      owasp:
        - "A4:2017-XML External Entities (XXE)"
        - "A05:2021-Security Misconfiguration"
      security-severity: "MEDIUM"
    severity: "WARNING"
