# yamllint disable
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# yamllint enable
---
rules:
- id: "ruby_mass_assignment_rule-UnprotectedMassAssign"
  patterns:
  - pattern-either:
    - pattern: |
        $MOD.new(params[$CODE])
    - pattern: |
        $MOD.new(..., params[$CODE], :without_protection => true, ...)
  - pattern-not-inside: |
      attr_accessible $VAR
      ...
      $MOD.new(params[$CODE])
  message: |
    Checks for calls to `without_protection` during mass assignment (which 
    allows record creation from hash values). This can lead to users bypassing 
    permissions protections. Using `:without_protection => true` makes your 
    application vulnerable to attackers who may craft malicious requests to 
    modify sensitive attributes, leading to unauthorized access or data 
    manipulation. For Rails 4 and higher, mass protection is on by default.
    
    To mitigate this issue, don't use :without_protection => true. Instead, 
    configure attr_accessible to control attribute access.
    
    Secure Code Example:
    ```
    class User < ActiveRecord::Base
      # Only name and email can be updated via mass assignment
      attr_accessible :name, :email

      # admin and account_id are not listed here, so they cannot be mass-assigned
    end
    ```
  languages:
  - "ruby"
  severity: "WARNING"
  metadata:
    category: "security"
    shortDescription: "Improperly controlled modification of dynamically-determined object attributes"
    cwe: "CWE-915"
    owasp:
    - "A6:2017-Security Misconfiguration"
    - "A08:2021-Software and Data Integrity Failures"
    security-severity: "MEDIUM"
    technology:
    - "ruby"
    - "rails"
    references:
    - "https://cheatsheetseries.owasp.org/cheatsheets/Mass_Assignment_Cheat_Sheet.html"
    - "https://github.com/semgrep/semgrep-rules/blob/develop/ruby/lang/security/unprotected-mass-assign.yaml"
